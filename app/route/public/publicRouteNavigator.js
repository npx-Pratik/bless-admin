import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
// import {AppRoute} from '../authRoute';
import { Text } from '@ui-kitten/components';

// Public Screens
// import LoginScreen from '../../screen/login/loginScreen';
// import HomeScreen from '../../screen/homescreen/homescreen';
// import HomePage from '../../screen/homepage/homepage';
// import Forgot from '../../screen/forgot/forgot';

const Stack = createStackNavigator();

export const PublicRouteNavigator = (props) => (
  <Text>Public</Text>
  // <Stack.Navigator>
  //   <Stack.Screen
  //     name={AppRoute.HOMEPAGE}
  //     options={{
  //       headerShown: false,
  //     }}>
  //     {(homepageProps) => <HomePage {...homepageProps} />}
  //   </Stack.Screen>
  //   <Stack.Screen
  //     name={AppRoute.HOME}
  //     options={{
  //       headerShown: false,
  //     }}>
  //     {(homeProps) => <HomeScreen {...homeProps} />}
  //   </Stack.Screen>
  //   <Stack.Screen
  //     name={AppRoute.SIGN_IN}
  //     options={{
  //       headerShown: false,
  //     }}>
  //     {(loginProps) => <LoginScreen {...loginProps} />}
  //   </Stack.Screen>
  //   <Stack.Screen
  //     name={AppRoute.FORGOT}
  //     options={{
  //       headerShown: false,
  //     }}>
  //     {(forgotprops) => <Forgot {...forgotprops} />}
  //   </Stack.Screen>
  // </Stack.Navigator>
);
