import React, { useState } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { AppRoute } from './authRoute';
import { PublicRouteNavigator } from './public/publicRouteNavigator';
import { PrivateNavigator } from './private/privateRouteNavigator';
// import AsyncStorage from '@react-native-community/async-storage';

const Stack = createStackNavigator();

export const AppNavigator = props => {
  // const [token, setToken] = useState('');
  // AsyncStorage.getItem('token').then((value) => {
  //   setToken(value)
  // })
  return (
    <Stack.Navigator {...props} headerMode="none">
      {/* {token ? */}
      <Stack.Screen name="Private" headerMode="modal">
        {props => <PrivateNavigator {...props} />}
      </Stack.Screen>
      {/* : */}
      <Stack.Screen name="Public">
        {props => <PublicRouteNavigator {...props} />}
      </Stack.Screen>
      {/* } */}
    </Stack.Navigator>
  );
};
