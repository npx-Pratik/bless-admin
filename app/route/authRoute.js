export const AppRoute = {
  PUBLIC: 'Public',
  SIGN_IN: 'Sign In',
  SIGN_UP: 'Sign Up',
  HOME: 'Home',
  DETAILSTABELE: "Details Table",
  BUTTONDEFAULTEXAMPLE: "Button Default Example",
  HOMEPAGE: 'Home Page',
  RESET_PASSWORD: 'Reset Passrord',
  FORGOT: 'Forgot',
  PRIVATE: 'Private',
  PROFILE: 'Profile',
  ABOUT: 'About',
  TODO_IN_PROGRESS: 'Progess',
  TODO_DONE: 'Done',
  STUDENT: 'Student',
  PARENT: 'Parent'
};
