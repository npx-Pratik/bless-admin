import React, { useState } from 'react';
import { StyleSheet, View, TouchableWithoutFeedback } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Icon, Input } from '@ui-kitten/components';
import Button from '../../components/Button';
import { emailValidator, passwordValidator } from '../../core/utils';
import axios from 'axios';
import AwesomeAlert from 'react-native-awesome-alerts';
import { AuthContext } from '../../context/context';

const AlertIcon = (props) => (
    <Icon {...props} name='alert-circle-outline' />
);

const LoginScreen = ({ navigation }) => {
    const { signIn } = React.useContext(AuthContext);
    const [email, setEmail] = useState({ value: '', error: '' });
    const [password, setPassword] = useState({ value: '', error: '' });
    const [showAlert, setMessage] = useState(false)
    const [error, setError] = useState(false)
    const [alert, setAlert] = useState('');
    const [secureTextEntry, setSecureTextEntry] = React.useState(true);

    const toggleSecureEntry = () => {
        setSecureTextEntry(!secureTextEntry);
    };

    const renderIcon = (props) => (
        <TouchableWithoutFeedback onPress={toggleSecureEntry}>
            <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'} />
        </TouchableWithoutFeedback>
    );
    const _onLoginPressed = () => {
        const emailError = emailValidator(email.value);
        const passwordError = passwordValidator(password.value);
        setMessage(true);
        if (emailError || passwordError) {
            setEmail({ ...email, error: emailError });
            setPassword({ ...password, error: passwordError });
            setMessage(false)
            return;
        }
        console.log("emial", email);
        console.log("pass", password);
        axios.post("http://localhost:8000/api/v1/user/login", {
            email: email.value,
            password: password.value
        }
        ).then(async (response) => {
            const respData = response.data.data.token;
            // const role = response.data.data.role;
            // const { name, email, role } = response.data.data.user;
            // const token = await AsyncStorage.setItem('token', respData);
            // await AsyncStorage.setItem('name', name);
            // await AsyncStorage.setItem('email', email);
            // const userName = await AsyncStorage.setItem('user', role);
            setMessage(false)
            await signIn(navigation, response.data.data);
        }).catch((error) => {
            console.log("data", error)
            console.log("data", JSON.stringify(error))
            setError(true);
            setAlert(JSON.stringify(error.message));
        });

    };
    const _onForgotPress = () => {
        navigation.navigate('SignUpScreen');
    }
    return (
        <View style={styles.containerStyle}>
            <Input
                placeholder={'Email'}
                label="Email"
                style={styles.InputStyle}
                value={email.value}
                size='medium'
                onChangeText={(text) => setEmail({ value: text, error: '' })}
                error={!!email.error}
                caption={email.error}
                autoCapitalize="none"
                autoCompleteType="email"
                textContentType="emailAddress"
                keyboardType="email-address"
            >
            </Input>
            <Input
                placeholder={'Password'}
                label="Password"
                size='medium'
                value={password.value}
                onChangeText={(text) => setPassword({ value: text, error: '' })}
                error={!!password.error}
                caption={password.error}
                captionIcon={AlertIcon}
                style={styles.InputStyle}
                accessoryRight={renderIcon}
                secureTextEntry={secureTextEntry}
            ></Input>
            <Button
                type="primary"
                size={'large'}
                style={{ margin: 16 }}
                onPress={() => {
                    _onLoginPressed();
                }}>
                Login
        </Button>
            <Button
                type="primary"
                size={'large'}
                style={{ margin: 16 }}
                onPress={() => {
                    _onForgotPress();
                }}>
                Forgot Password
        </Button>
            <AwesomeAlert
                showProgress={showAlert}
                message="Loading..."
            />
            <AwesomeAlert
                show={error}
                title="Error"
                message={alert}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                showCancelButton={false}
                showConfirmButton={true}
                confirmText="OK"
                confirmButtonColor="#DD6B55"
                onConfirmPressed={() => {
                    setError(false);
                    setAlert('');
                }}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    containerStyle: {
        width: '75%',
        marginVertical: 12,
        marginLeft: 40,
        marginTop: 100,
    },
    InputStyle: {
        marginLeft: 20
    }
});

export default LoginScreen;
