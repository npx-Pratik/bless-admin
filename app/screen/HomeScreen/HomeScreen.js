import React from 'react';
import { View, StyleSheet, FlatList, Dimensions, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';
import { useTheme } from '@react-navigation/native';
import { Button, Divider, Icon, List, ListItem, Layout, Text, TopNavigation, TopNavigationAction, Card, Avatar } from '@ui-kitten/components';
import { InfoIcon } from '../../assets/icons';
import { FlatGrid } from 'react-native-super-grid';
const BackIcon = (props) => (
  <Icon {...props} name='arrow-back' />
);

const SettingsIcon = (props) => (
  <Icon {...props} name='settings' />
);
const numColumn = 2;
const WIDTH = Dimensions.get('window').width;

const data = new Array(20).fill({
  title: 'Title for Item',
  description: 'Description for Item',
});

const HomeScreen = ({ navigation }) => {
  const [items, setItems] = React.useState([
    { name: 'Profile', code: '#1abc9c', nav: 'Profile', icon: 'person-add' },
    { name: 'Attendance', code: '#2ecc71', nav: 'Attendance', icon: 'people' },
    { name: 'Time Table', code: '#3498db', nav: 'Profile', icon: 'calendar' }, ,
    { name: 'Interview', code: '#9b59b6', nav: 'Profile', icon: 'people' },
    { name: 'Leave', code: '#34495e', nav: 'Profile', icon: 'log-out-outline' },
    { name: 'Salary', code: '#16a085', nav: 'Profile', icon: 'person-add' },
    { name: 'Syllabus', code: '#27ae60', nav: 'Profile', icon: 'layers' },
    { name: 'Notification', code: '#27ae60', nav: 'Profile', icon: 'bell-outline' },
  ]);

  return (
    <>
      <FlatGrid
        itemDimension={130}
        data={items}
        style={styles.gridView}
        // staticDimension={300}
        // fixed
        spacing={10}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => navigation.navigate(item.nav)}>
            <View style={[styles.itemContainer, { backgroundColor: item.code }]}>
              <Icon
                style={styles.itemCode}
                fill='#FFFFFF'
                name={item.icon}
              />
              <Text style={styles.itemName}>{item.name}</Text>

            </View>
          </TouchableOpacity>
        )}
      />
    </>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  gridView: {
    marginTop: 10,
    flex: 1,
  },
  itemContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    // justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 85,
  },
  itemName: {
    fontSize: 16,
    color: '#FFFFFF',
    fontWeight: '600',
    // marginLeft: 10,
    // alignItems: 'center',
    // justifyContent: 'center',
    // alignContent: 'center',
  },
  itemCode: {
    fontSize: 6,
    width: 32,
    height: 32,
    color: '#fff',
  },
});
