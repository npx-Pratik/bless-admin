import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Icon, TopNavigationAction, TopNavigation, Avatar, Divider, Datepicker, Text, Button, Card, List, ListItem } from '@ui-kitten/components';
import { createStackNavigator } from '@react-navigation/stack';

const AttendanceStack = createStackNavigator();
const data = [{
    title: 'Item',
    status: 'Present',
    description: 'hello'
}, {
    title: 'Item',
    status: 'Present',
    description: 'hello'
}, {
    title: 'Item',
    status: 'Absent',
    description: 'hello'
}, {
    title: 'Item',
    status: 'Absent',
    description: 'hello'
}, {
    title: 'Item',
    status: 'Present',
    description: 'hello'
}, {
    title: 'Item',
    status: 'Absent',
    description: 'hello'
}, {
    title: 'Item',
    status: 'Present',
    description: 'hello'
}, {
    title: 'Item',
    status: 'Present',
    description: 'hello'
}, {
    title: 'Item',
    status: 'Absent',
    description: 'hello'
}, {
    title: 'Item',
    status: 'Present',
    description: 'hello'
}];
const DemoScreen = ({ navigation }) => {
    const renderItemAccessory = (e, props) => {
        const color = props.status === 'Present' ? 'success' : 'danger';
        return < Button size='tiny' status={color}>{props.status}</Button>
    };
    const [date, setDate] = React.useState(new Date());
    const renderItemIcon = (props) => (
        <Avatar style={styles.avatar} size='giant' source={{
            uri: 'https://api.adorable.io/avatars/50/abott@adorable.png'
        }} />
    );

    const renderItem = ({ item, index }) => (
        <ListItem
            title={`${item.title} ${index + 1}`}
            description={`${item.description} ${index + 1}`}
            accessoryLeft={renderItemIcon}
            accessoryRight={(e) => renderItemAccessory(e, item)}
        />
    );

    return (
        <>
            <View style={styles.DateStyle}>
                <Text category='h6'>
                    Selected date: {date.toLocaleDateString()}
                </Text>
                <Datepicker
                    date={date}
                    onSelect={nextDate => setDate(nextDate)}
                />
            </View>
            <List
                style={styles.container}
                ItemSeparatorComponent={Divider}
                data={data}
                renderItem={renderItem}
            />
        </>
    );
};

const AttendanceScreen = ({ navigation }) => {

    return (
        <AttendanceStack.Navigator screenOptions={{
            headerStyle: {
                backgroundColor: '#009387',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold'
            }
        }}>
            <AttendanceStack.Screen name="Attendance" component={DemoScreen} options={{
                title: 'Attendance',
                headerLeft: () => (
                    <Icon
                        style={styles.icon}
                        fill='#FFF'
                        name='menu-outline'
                        onPress={() => navigation.openDrawer()}
                    />)
            }} />
            {/* <AttendanceStack.Screen name="Demo" component={DemoScreen} /> */}
        </AttendanceStack.Navigator>
    );
};

const styles = StyleSheet.create({
    containerStyle: {
        width: '75%',
        marginVertical: 12,
        marginLeft: 40,
        marginTop: 100,
    },
    InputStyle: {
        marginLeft: 20
    },
    icon: {
        width: 32,
        height: 32,
        marginLeft: 10
    },
    DateStyle: {
        width: "50%",
        marginLeft: 20
    },
    avatar: {
        width: 40,
        height: 40
    }
});


export default AttendanceScreen;
