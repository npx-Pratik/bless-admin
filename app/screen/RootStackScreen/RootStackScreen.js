import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import SplashScreen from '../SplashScreen/SplashScreen';
import LoginScreen from '../LoginScreen/LoginScreen';
import ForgotScreen from '../ForgotScreen/ForgotScreen';

const RootStack = createStackNavigator();

const RootStackScreen = ({ navigation }) => (
    <RootStack.Navigator headerMode='none'>
        <RootStack.Screen name="SplashScreen" component={SplashScreen} />
        <RootStack.Screen name="SignInScreen" component={LoginScreen} />
        <RootStack.Screen name="SignUpScreen" component={ForgotScreen} />
    </RootStack.Navigator>
);

export default RootStackScreen;