import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';

const slides = [
    {
        key: 1,
        title: 'Title 1',
        text: 'Description.\nSay something cool',
        image: require('../../assets/1.jpg'),
        backgroundColor: '#59b2ab',
    },
    {
        key: 2,
        title: 'Title 2',
        text: 'Other cool stuff',
        image: require('../../assets/2.jpg'),
        backgroundColor: '#febe29',
    },
];
const styles = StyleSheet.create({
    slide: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'blue',
    },
    image: {
        width: 320,
        height: 320,
        marginVertical: 32,
    },
    text: {
        color: 'rgba(255, 255, 255, 0.8)',
        textAlign: 'center',
    },
    title: {
        fontSize: 22,
        color: 'white',
        textAlign: 'center',
    },
});
export default class SlashScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showRealApp: false,
        };
    }
    async componentDidMount() {
        // const token = await AsyncStorage.getItem('token');
        // if (token != null) {
        //   this.props.navigation.navigate('Private');
        // }
        // else {
        //   this.props.navigation.navigate('Home Page');
        // }
    }

    _renderItem = ({ item }) => {
        return (
            <View style={styles.slide}>
                {/* <Text style={styles.title}>{item.title}</Text> */}
                <Image source={item.image} />
                <Text style={styles.text}>{item.text}</Text>
            </View>
        );
    };
    _onDone = () => {
        // User finished the introduction. Show real app through
        // navigation or simply by controlling state
        this.setState({ showRealApp: true });
        this.props.navigation.navigate('SignInScreen');
        console.log("home page")

    };
    render() {
        return (
            <AppIntroSlider
                renderItem={this._renderItem}
                data={slides}
                onDone={this._onDone}
            />
        );
    }
}
