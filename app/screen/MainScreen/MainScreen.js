import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Icon, TopNavigationAction, TopNavigation, Text, Button } from '@ui-kitten/components';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from "../HomeScreen/HomeScreen";

const HomeStack = createStackNavigator();

const DemoScreen = ({ navigation }) => {

    return (
        <View style={styles.container}>
            {/* <StatusBar barStyle= { theme.dark ? "light-content" : "dark-content" }/> */}
            <Text>Home Screen</Text>
            <Button
                title="Go to details screen"
                onPress={() => navigation.navigate("Home")}
            />
        </View>
    );
};

const MainScreen = ({ navigation }) => {

    return (
        <HomeStack.Navigator screenOptions={{
            headerStyle: {
                backgroundColor: '#009387',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold'
            }
        }}>
            <HomeStack.Screen name="Home" component={HomeScreen} options={{
                title: 'Dashboard',
                headerLeft: () => (
                    <Icon
                        style={styles.icon}
                        fill='#FFF'
                        name='menu-outline'
                        onPress={() => navigation.openDrawer()}
                    />)
            }} />
            <HomeStack.Screen name="Demo" component={DemoScreen} />
        </HomeStack.Navigator>
    );
};

const styles = StyleSheet.create({
    containerStyle: {
        width: '75%',
        marginVertical: 12,
        marginLeft: 40,
        marginTop: 100,
    },
    InputStyle: {
        marginLeft: 20
    },
    icon: {
        width: 32,
        height: 32,
        marginLeft: 10
    },
});


export default MainScreen;
