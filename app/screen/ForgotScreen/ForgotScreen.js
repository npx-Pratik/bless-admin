import React, { useState } from 'react';
import { StyleSheet, View, TouchableWithoutFeedback } from 'react-native';
// import AsyncStorage from '@react-native-community/async-storage'
import { Input, Icon } from '@ui-kitten/components';
import Button from '../../components/Button';
import axios from 'axios';
import AwesomeAlert from 'react-native-awesome-alerts';

const AlertIcon = (props) => (
    <Icon {...props} name='alert-circle-outline' />
);
const ForgotScreen = ({ navigation }) => {
    const [email, setEmail] = useState({ value: '', error: '' });
    const [password, setPassword] = useState({ value: '', error: '' });
    const [passCode, setCode] = useState({ value: '', error: '' });
    const [step, setStep] = useState({ value: 'email' });
    const [viewEmail, setViewEmail] = useState(true);
    const [viewCode, setViewCode] = useState(false);
    const [viewPassword, setViewPassword] = useState(false);
    const [secureTextEntry, setSecureTextEntry] = React.useState(true);
    const [showAlert, setMessage] = useState(false)
    const [error, setError] = useState(false)
    const [alert, setAlert] = useState('');

    const toggleSecureEntry = () => {
        setSecureTextEntry(!secureTextEntry);
    };

    const renderIcon = (props) => (
        <TouchableWithoutFeedback onPress={toggleSecureEntry}>
            <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'} />
        </TouchableWithoutFeedback>
    );

    const _onLoginPressed = async () => {
        setAlert(true)
        let url = "http://10.0.2.2:8000/api/v1/user/forgotPassword";
        let formData = {
            email: email.value
        };
        if (step.value === "code") {
            url = "http://10.0.2.2:8000/api/v1/user/verifyResetCode";
            formData.passCode = passCode.value;
        }

        if (step.value === "pass") {
            url = "http://10.0.2.2:8000/api/v1/user/resetPassword";
            formData.passCode = passCode.value;
            formData.password = password.value;
        }
        await axios.post(url, formData
        ).then((response) => {
            if (step.value === "email") {
                setViewEmail(false);
                setViewCode(true);
                setStep({ value: 'code' });
            }
            if (step.value === "code") {
                setViewCode(false);
                setViewPassword(true)
                setStep({ value: 'pass' });
            }
            if (password.value != '') {
                setTimeout(() => {
                    return navigation.navigate('SignUpScreen');
                }, 1000);
            }
            setAlert(false)
        }).catch((error) => {
            setError(true);
            setAlert(JSON.stringify(error.message));
        });


    };

    return (
        <>
            <View style={styles.containerStyle}>

                {viewEmail && <Input
                    placeholder='Email'
                    label="Email"
                    // returnKeyType="next"
                    value={email.value}
                    size='medium'
                    onChangeText={(text) => setEmail({ value: text, error: '' })}
                    error={!!email.error}
                    caption={email.error}
                    autoCapitalize="none"
                    autoCompleteType="email"
                    textContentType="emailAddress"
                    keyboardType="email-address"
                ></Input>}
                {viewCode && <Input
                    placeholder={'Enter Code'}
                    label="Enter Code"
                    size='medium'
                    value={passCode.value}
                    onChangeText={(text) => setCode({ value: text, error: '' })}
                    error={!!passCode.error}
                    caption={passCode.error}
                ></Input>}
                {viewPassword && <Input
                    placeholder={'Password'}
                    label="Password"
                    size='medium'
                    value={password.value}
                    onChangeText={(text) => setPassword({ value: text, error: '' })}
                    error={!!password.error}
                    caption={password.error}
                    captionIcon={AlertIcon}
                    accessoryRight={renderIcon}
                    secureTextEntry={secureTextEntry}
                ></Input>
                }
                <Button
                    type="primary"
                    size={'large'}
                    style={{ marginTop: 16 }}
                    onPress={() => {
                        _onLoginPressed();
                    }}>
                    Submit
        </Button>
                <AwesomeAlert
                    showProgress={showAlert}
                    useNativeDriver={showAlert}
                    message="Loading..."
                />
                <AwesomeAlert
                    show={error}
                    title="Error"
                    message={alert}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={false}
                    showConfirmButton={true}
                    confirmText="OK"
                    confirmButtonColor="#DD6B55"
                    onConfirmPressed={() => {
                        setError(false);
                        setAlert('');
                    }}
                />
            </View>
        </>
    );
};

const styles = StyleSheet.create({
    containerStyle: {
        width: '75%',
        marginVertical: 12,
        marginLeft: 40,
        marginTop: 90,
    },
    InputStyle: {
        marginLeft: 15
    }
});

export default ForgotScreen;
