import React from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';
import { Icon } from '@ui-kitten/components';
import { createStackNavigator } from '@react-navigation/stack';

const ProfileStack = createStackNavigator();

const Profile = () => {
    return (
        <View style={styles.container}>
            <Text>Profile Screen</Text>
            <Button
                title="Click Here"
                onPress={() => alert('Button Clicked!')}
            />
        </View>
    );
}



const ProfileScreen = ({ navigation }) => {
    return (
        <ProfileStack.Navigator screenOptions={{
            headerStyle: {
                backgroundColor: '#009387',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold'
            }
        }}>
            <ProfileStack.Screen name="Profile" component={Profile} options={{
                title: 'Profile',
                headerLeft: () => (
                    <Icon
                        style={styles.icon}
                        fill='#FFF'
                        name='menu-outline'
                        onPress={() => navigation.openDrawer()}
                    />)
            }} />
            <ProfileStack.Screen name="Profile1" component={Profile} options={{
                title: 'Profile',
                headerLeft: () => (
                    <Icon
                        style={styles.icon}
                        fill='#FFF'
                        name='menu-outline'
                        onPress={() => navigation.openDrawer()}
                    />)
            }} />
        </ProfileStack.Navigator>
    );
};

export default ProfileScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        width: 32,
        height: 32,
        marginLeft: 10
    },
});
