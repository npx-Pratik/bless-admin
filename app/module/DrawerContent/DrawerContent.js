import React from 'react';
import { View, StyleSheet } from 'react-native';
import {
    Avatar,
    Text,
    Drawer as UIKittenDrawer,
    IndexPath,
    DrawerItem
} from '@ui-kitten/components';
import { HomeIcon, InfoIcon, LogoutIcon } from '../../assets/icons';

import { AuthContext } from '../../context/context';

const DrawerContent = ({ navigation, state }) => {
    const { signOut } = React.useContext(AuthContext);
    const logoutCheck = (index) => {
        console.log("index", index.row)
        if (index.row === 4) {
            // const { signOut } = authContext;
            // console.log("sdfds", authContext)
            signOut(navigation)
            // navigation.navigate('Home');
            // console.log("logout")
            return
        }
        navigation.navigate(state.routeNames[index.row])
    }
    return (
        <>
            <View style={{ flexDirection: 'row', marginTop: 15 }}>
                <Avatar style={styles.avatar} size='giant' source={{
                    uri: 'https://api.adorable.io/avatars/50/abott@adorable.png'
                }} />
                <View style={{ marginLeft: 15, flexDirection: 'column' }}>
                    <Text style={styles.title}>John Doe</Text>
                    <Text style={styles.caption}>@j_doe</Text>
                </View>
            </View>
            <UIKittenDrawer
                selectedIndex={new IndexPath(state.index)}
                onSelect={index => logoutCheck(index)}>
                <DrawerItem title='Home' accessoryLeft={HomeIcon} />
                <DrawerItem title='Profile' accessoryLeft={InfoIcon} />
                <DrawerItem title='Support' accessoryLeft={HomeIcon} />
                <DrawerItem title='Setting' accessoryLeft={HomeIcon} />
                <DrawerItem title='Logout' accessoryLeft={LogoutIcon} />
            </UIKittenDrawer>
        </>
    );
};

const styles = StyleSheet.create({
    drawerContent: {
        flex: 1,
    },
    userInfoSection: {
        paddingLeft: 20,
    },
    title: {
        fontSize: 16,
        marginTop: 3,
        fontWeight: 'bold',
    },
    caption: {
        fontSize: 14,
        lineHeight: 14,
    },
    row: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    paragraph: {
        fontWeight: 'bold',
        marginRight: 3,
    },
    drawerSection: {
        marginTop: 15,
    },
    bottomDrawerSection: {
        marginBottom: 15,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1
    },
    preference: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 16,
    },
});

export default DrawerContent;





// import React from 'react';
// import { View, StyleSheet } from 'react-native';
// import {
//     Avatar,
//     Title,
//     Caption,
//     Paragraph,
//     Drawer,
//     Icon,
//     DrawerItem
// } from '@ui-kitten/components';
// import {
//     DrawerContentScrollView,
// } from '@react-navigation/drawer';

// import { AuthContext } from '../../context/context';

// const ForwardIcon = (props) => (
//     <Icon {...props} name='arrow-ios-forward' />
// );
// const DrawerContent = (props) => {

//     // const paperTheme = useTheme();

//     const { signOut } = React.useContext(AuthContext);

//     return (
//         <View style={{ flex: 1 }}>
//             {/* <DrawerContentScrollView {...props}> */}
//             <View style={styles.drawerContent}>
//                 <View style={styles.userInfoSection}>
//                     <View style={{ flexDirection: 'row', marginTop: 15 }}>
//                         <Avatar
//                             // source={{
//                             //     uri: 'https://api.adorable.io/avatars/50/abott@adorable.png'
//                             // }}
//                             size='large'
//                         />
//                         <View style={{ marginLeft: 15, flexDirection: 'column' }}>
//                             <Title style={styles.title}>John Doe</Title>
//                             <Caption style={styles.caption}>@j_doe</Caption>
//                         </View>
//                     </View>

//                     <View style={styles.row}>
//                         <View style={styles.section}>
//                             <Paragraph style={[styles.paragraph, styles.caption]}>80</Paragraph>
//                             <Caption style={styles.caption}>Following</Caption>
//                         </View>
//                         <View style={styles.section}>
//                             <Paragraph style={[styles.paragraph, styles.caption]}>100</Paragraph>
//                             <Caption style={styles.caption}>Followers</Caption>
//                         </View>
//                     </View>
//                 </View>

//                 <Drawer style={styles.drawerSection}>
//                     <DrawerItem
//                         accessoryLeft={ForwardIcon}
//                         title="Home"

//                     />
//                     <DrawerItem
//                         accessoryLeft={ForwardIcon}
//                         title="Profile"
//                     />
//                     <DrawerItem
//                         accessoryLeft={ForwardIcon}
//                         title="Bookmarks"

//                     />
//                     <DrawerItem
//                         accessoryLeft={ForwardIcon}
//                         title="Settings"

//                     />
//                     <DrawerItem

//                         accessoryLeft={ForwardIcon}
//                         title="Support"

//                     />
//                 </Drawer>
//             </View>
//             {/* </DrawerContentScrollView> */}
//             <Drawer style={styles.bottomDrawerSection}>
//                 <DrawerItem
//                     accessoryLeft={ForwardIcon}
//                     title="Sign Out"

//                 />
//             </Drawer>
//         </View>
//     );
// }

// export default DrawerContent;
