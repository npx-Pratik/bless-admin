/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState, useMemo } from 'react';
import { ApplicationProvider, Layout, Text, IconRegistry, Avatar, Title, Caption, Paragraph, Drawer as UIKittenDrawer, IndexPath, DrawerItem } from '@ui-kitten/components';
import * as eva from '@eva-design/eva';
import { SafeAreaLayout, SaveAreaInset } from './app/module/safeArea/safeAreaLayout';
import { AppNavigator } from "./app/route/appNavigator";
import { NavigationContainer } from '@react-navigation/native';
import { View, ActivityIndicator, StyleSheet } from 'react-native';
import RootStackScreen from './app/screen/RootStackScreen/RootStackScreen';
import { AuthContext } from './app/context/context';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import MainScreen from "./app/screen/MainScreen/MainScreen";
import ProfileScreen from "./app/screen/ProfileScreen/ProfileScreen";
import SupportScreen from "./app/screen/SupportScreen/SupportScreen";
import SettingScreen from "./app/screen/SettingScreen/SettingScreen";
import DrawerContent from './app/module/DrawerContent/DrawerContent';
import { HomeIcon, InfoIcon, LogoutIcon } from './app/assets/icons';
import AsyncStorage from '@react-native-community/async-storage';
import LoginScreen from './app/screen/LoginScreen/LoginScreen';
import AttendanceScreen from './app/screen/Attendance/AttendanceScreen';
import config from './config';
const Drawer = createDrawerNavigator();
const App = () => {

  console.log(config);
  const [isLoading, setIsLoading] = useState(true);
  const [userToken, setUserToken] = useState(null);
  const [userName, setUserName] = useState(null);
  const authContext = useMemo(() => ({
    signIn: async (navigation, foundUser) => {
      // setUserToken('fgkj');
      // setIsLoading(false);
      const { token, role, name, email } = foundUser;
      const userToken = await AsyncStorage.setItem('token', token);
      await AsyncStorage.setItem('name', name);
      await AsyncStorage.setItem('email', email);
      const userName = await AsyncStorage.setItem('user', role);
      setUserToken(userToken);
      setUserName(userName)
      setIsLoading(false);
      navigation.navigate('Home');
      // const userToken = String(foundUser[0].userToken);
      // const userName = foundUser[0].username;

      // try {
      //   await AsyncStorage.setItem('userToken', userToken);
      // } catch (e) {
      //   console.log(e);
      // }
      // // console.log('user token: ', userToken);
      // dispatch({ type: 'LOGIN', id: userName, token: userToken });
    },
    signOut: async (navigation) => {
      // console.log("na", navigation)
      setUserToken(null);
      setUserName(null);
      setIsLoading(false);
      await AsyncStorage.removeItem('token');
      await AsyncStorage.removeItem('name');
      await AsyncStorage.removeItem('email');
      await AsyncStorage.removeItem('user');
      navigation.navigate('SignInScreen');
      // try {
      // } catch (e) {
      //   console.log(e);
      // }
      // dispatch({ type: 'LOGOUT' });
    },
  }), []);

  useEffect(() => {
    setTimeout(async () => {
      setIsLoading(false);
      let userToken;
      userToken = null;
      try {
        userToken = await AsyncStorage.getItem('userToken');
        console.log('user token: ', userToken);
      } catch (e) {
        console.log(e);
      }
      console.log('user token: ', userToken);

    }, 1000);
  }, [])

  if (isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="large" />
      </View>
    );
  }

  return (
    <>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider {...eva} theme={eva.light}>
        <AuthContext.Provider value={authContext}>
          <NavigationContainer>
            <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}>
              <Drawer.Screen name="Home" component={MainScreen} options={{ title: 'Dashboard', drawerIcon: HomeIcon }} />
              <Drawer.Screen name="Profile" component={ProfileScreen} options={{ title: 'Profile', drawerIcon: HomeIcon }} />
              <Drawer.Screen name="Support" component={SupportScreen} options={{ title: 'Support', drawerIcon: InfoIcon }} />
              <Drawer.Screen name="Setting" component={SettingScreen} options={{ title: 'Setting', drawerIcon: InfoIcon }} />
              <Drawer.Screen name="Attendance" component={AttendanceScreen} options={{ title: 'Attendance', drawerIcon: InfoIcon }} />
              <Drawer.Screen name="SignInScreen" component={LoginScreen} options={{ title: 'Logout', drawerIcon: InfoIcon }} />
            </Drawer.Navigator>
          </NavigationContainer>
        </AuthContext.Provider>
      </ApplicationProvider>
    </>
  );
};

export default App;
const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingLeft: 20,
  },
  title: {
    fontSize: 16,
    marginTop: 3,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  row: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 15,
  },
  paragraph: {
    fontWeight: 'bold',
    marginRight: 3,
  },
  drawerSection: {
    marginTop: 15,
  },
  bottomDrawerSection: {
    marginBottom: 15,
    borderTopColor: '#f4f4f4',
    borderTopWidth: 1
  },
  preference: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
  avatar: {
    margin: 20,
  }
});
