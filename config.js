import { API_URL, ANALYTICS_KEY } from 'react-native-dotenv';

export default {
    API_URL,
    ANALYTICS_KEY,
};